import java.util.Scanner;

public class Array9 {
    public static void main(String[] args) {
        int arr[] = new int[3];
        Scanner kb = new Scanner(System.in);
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = kb.nextInt();

        }
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");

        }
        System.out.println();

        System.out.print("please input search value: ");
        int searchValue = kb.nextInt();
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == searchValue) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            System.out.print("found at index: "+ index);

        } else {
            System.out.print("not found");
        }

    }

}
