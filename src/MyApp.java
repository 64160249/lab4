import java.util.Scanner;

public class MyApp {
    static void printWelcome() {
        System.out.println("Welcome to my app!!!");
    }

    static void printMenu() {
        System.out.println("--Menu--");
        System.out.println("1. Print Hello World N times");
        System.out.println("2. Add 2 number");
        System.out.println("3. Exit");

    }

    static int inputChoice() {
        int choice;
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Please input your choice(1-3): ");
            choice = kb.nextInt();
            if (choice >= 1 && choice <= 3) {
                return choice;
            }
            System.out.println("Error: Please input between 1-3");
        }

    }

    public static void main(String[] args) {
        int choice = 0;
        while (true) {
            printWelcome();
            printMenu();
            choice = inputChoice();
            process(choice);
        }
    }

   static void process(int choice) {
    switch (choice) {
        case 1:
            printHelloWorld();

            break;
        case 2:
            addTwoNumbers();

            break;
        case 3:
            exitProgram();
            break;

    }
    }

    static void addTwoNumbers() {
        Scanner kb = new Scanner(System.in);
        int first, second;
        int result;
        System.out.print("Please input first number: ");
        first = kb.nextInt();
        System.out.print("Please input second number: ");
        second = kb.nextInt();
        result = first + second;
        System.out.print("Result = " + result);
        System.out.println();
    }

    static void printHelloWorld() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input time: ");
        int time = kb.nextInt();
        for (int i = 0; i < time; i++) {
            System.out.println("Hello World!!!");

        }
    }

    static void exitProgram() {
        System.out.println("Bye!!!");
        System.exit(0);
    }

}
