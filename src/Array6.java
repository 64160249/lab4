import java.util.Scanner;

public class Array6 {
    public static void main(String[] args) {
        int arr[] = new int[3];
        int sum = 0;
        double avg =0;
        Scanner kb = new Scanner(System.in);

        for (int i = 0; i < arr.length; i++) {
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = kb.nextInt();
            sum += arr[i];
            avg = ((double)(sum)/arr.length);

        }
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");

        }
        System.out.println();
        System.out.print("sum = " + sum);
        System.out.println();
        System.out.print("avg = " + avg);

    }

}
